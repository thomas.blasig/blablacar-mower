package com.tblasig.mower;

import com.tblasig.mower.service.MowerController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;

/***
 * Main
 * Handle needed arg (input file path) and start the program.
 */
public class MowerApplication {
    private static final Logger LOG = LogManager.getLogger(MowerApplication.class.getName());

    public static void main(String[] args){
        if (args == null || args.length != 1) {
            LOG.debug("Program was not called with the input file path");
            throw new IllegalArgumentException("input file's path should be given => cmd {FILE_PATH}");
        }
        String filePath = args[0];
        new MowerController(new File(filePath)).start();
    }



}
