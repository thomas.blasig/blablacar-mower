package com.tblasig.mower.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import com.google.common.base.Enums;
import com.tblasig.mower.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/***
 * Handles the input file and creates all the elements and the relation between them. (Lawn, mower, instructions).
 */
public class MowerController {

    private static final Logger LOG = LogManager.getLogger(MowerController.class.getName());
    private final File inputFile;

    public MowerController(File inputFile) {
        this.inputFile = inputFile;
    }

    public void start() {
        try {
            Scanner sc = new Scanner(inputFile);
            Coordinates2D maxCoordinate = Coordinates2D.fromString(sc.nextLine());
            Lawn lawn = new Lawn(maxCoordinate);

            // Mower's intructions are in 2 lines so
            while (sc.hasNextLine()) {
                String mowerInstruction1 = sc.nextLine();

                Position position = Position.fromString(mowerInstruction1);
                Mower mower = new Mower(position, lawn);
                if (!sc.hasNext()) { // hasNext() and not hasNextLine() because we want to ensure there is no empty lines
                    throw new IllegalArgumentException(String.format("Mower has to be declared in 2 lines, but just 1 has be given for [%s]", mowerInstruction1));
                }
                String mowerInstruction2 = sc.nextLine();
                mower.setInstructions(
                        new LinkedList<>(
                                mowerInstruction2
                                        .chars()
                                        .mapToObj(c -> (char) c)
                                        .map(c -> Enums
                                                    .getIfPresent(Movement.class, Character.toString(c))
                                                    .toJavaUtil()
                                                    .orElseThrow(() -> // Control the error message displayed to the console (Like this the user can know which mower causes the issue)
                                                            new IllegalArgumentException(String.format("Incorrect instruction '%c' for Mower [%s]", c, mower.getPosition()))
                                                    ))
                                        .collect(Collectors.toList())
                        )
                );
                lawn.addMower(mower);
            }
            LOG.debug("Start mowing");
            System.out.println(lawn.startMowing());

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(String.format("file %s is not found", inputFile));
        }
    }

}
