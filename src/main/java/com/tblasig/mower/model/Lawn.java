package com.tblasig.mower.model;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

/***
 * Represents the lawn. Holds the logic of the mowing. Controls and synchronizes all the mowers together.
 * Handles the collision and the move outside of the grid as well.
 */
public class Lawn {

    private static final Logger LOG = LogManager.getLogger(Lawn.class.getName());
    public final static Coordinates2D MIN_COORDINATES = new Coordinates2D(0,0);

    private Coordinates2D maxCoordinates;
    private Coordinates2D minCoordinates = MIN_COORDINATES;

    /**
     * This set contains each mower's position in real time. It is useful to ensure that the next cell is available.
     * This set is protected by a Lock because each mower has to read and write in a 1 atomic transaction =>So Hashset is good (there s no need to have SynchronizedSet)
     */
    private Set<Position> busyCells = new HashSet<>();
    private ReentrantLock busyCellsLock = new ReentrantLock();

    private List<Mower> mowers = new ArrayList<>();

    public Lawn(Coordinates2D maxCoordinates) {
        this.maxCoordinates = maxCoordinates;
    }

    public void addMower(Mower mower) {
        if (!isCorrectCoordinate(mower.getPosition().getCoordinates())) {
            throw new IllegalArgumentException("Mower coordinates are not correct");
        }
        mowers.add(mower);
        if (busyCells.contains(mower.getPosition())) {
            throw new IllegalArgumentException("2 Mower shall not start in the same location");
        }
        busyCells.add(mower.getPosition());
    }


    public String startMowing() {
        ExecutorService es = Executors.newFixedThreadPool(mowers.size());
        List<String> result = new ArrayList<>();
        try {
            List<Future<Position>> futures = es.invokeAll(mowers);
            // futures.stream.map() would be cleaner here, but it leads to double catch block (one for invokeAll and one for future.get(), so I decided to not use it)
            for (Future<Position> future_: futures) {
                result.add(future_.get().toString());
            }
        } catch (ExecutionException | InterruptedException e) {
            Thread.currentThread().interrupt();
            LOG.error("An error has been caught during mowing");
            throw new RuntimeException("Failed to mow due to a thread issue");
        } finally {
            es.shutdown();
        }

        return String.join("\n", result);
    }

    public void lockLawn() {
        LOG.debug("ask to lock lawn");
        busyCellsLock.lock();
        LOG.debug("lawn is now locked");
    }

    public void unlockLawn() {
        LOG.debug("ask to unlock lawn");
        busyCellsLock.unlock();
        LOG.debug("lawn is now unlocked");
    }

    public boolean isPositionAvailable(Position position) {
        if (!busyCellsLock.isLocked()) {
            throw new RuntimeException("A lock is mandatory before accessing to this resource.");
        }
        return !busyCells.contains(position);
    }

    public void confirmNewPosition(Position previousPosition, Position newPosition) {
        if (!busyCellsLock.isLocked()) {
            throw new RuntimeException("A lock is mandatory before accessing to this resource.");
        }
        busyCells.remove(previousPosition);
        busyCells.add(newPosition);
    }

    public boolean isCorrectCoordinate(Coordinates2D coordinates2D) {
        return coordinates2D.isGreaterOrEqualsThan(minCoordinates) && !coordinates2D.isGreaterThan(maxCoordinates);
    }

    public Coordinates2D getMaxCoordinates() {
        return maxCoordinates;
    }

    public void setMaxCoordinates(Coordinates2D maxCoordinates) {
        this.maxCoordinates = maxCoordinates;
    }

    public Coordinates2D getMinCoordinates() {
        return minCoordinates;
    }

    public void setMinCoordinates(Coordinates2D minCoordinates) {
        this.minCoordinates = minCoordinates;
    }

    public List<Mower> getMowers() {
        return mowers;
    }

    public void setMowers(List<Mower> mowers) {
        this.mowers = mowers;
    }

}
