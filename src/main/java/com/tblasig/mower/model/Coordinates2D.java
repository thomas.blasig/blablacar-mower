package com.tblasig.mower.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * Represents 2D coordinates. Can be created from a String. Can be compared to other coordinates.
 */
public class Coordinates2D {

    public static final String REGEX_POSITION_ONLY = "(\\d+) (\\d+)";

    private int X;

    private int Y;

    public Coordinates2D(int x, int y) {
        X = x;
        Y = y;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        Y = y;
    }

    public boolean isGreaterThan(Coordinates2D coord) {
        return this.X > coord.getX() ||  this.Y > coord.getY();
    }

    public boolean isGreaterOrEqualsThan(Coordinates2D coord) {
        // If X is equals then Y has to be equals too
        return (this.X > coord.getX() ||  this.Y >coord.getY()) || (this.X == coord.getX() && this.Y == coord.getY());
    }

    @Override
    public String toString() {
        return "[" + X + ", " + Y + "]";
    }

    public static Coordinates2D fromString(String line) {
        Pattern pattern = Pattern.compile(REGEX_POSITION_ONLY);
        Matcher matcher = pattern.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Position is not well formatted");
        }
        int X = Integer.valueOf(matcher.group(1));
        int Y = Integer.valueOf(matcher.group(2));
        return new Coordinates2D(X, Y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates2D that = (Coordinates2D) o;
        return X == that.X &&
                Y == that.Y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }
}
