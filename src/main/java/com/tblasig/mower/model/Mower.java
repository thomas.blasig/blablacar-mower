package com.tblasig.mower.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.Callable;


/***
 * Represents a mower. Holds the position and the instructions. Has a reference to the lawn. (N mower -> 1 lawn).
 * When started, it computes its position until the instructions are empty and return the last one.
 */
public class Mower implements Callable<Position> {
    private static final Logger LOG = LogManager.getLogger(Mower.class.getName());

    private Position position;
    private Queue<Movement> instructions = new LinkedList<>();
    private Lawn lawn;


    public Mower(Position position, Lawn lawn) {
        this.position = position;
        this.lawn = lawn;
    }

    @Override
    public Position call() {
        Position newHypotheticalPosition = null;
        while (this.hasNextPosition()) {
            newHypotheticalPosition = this.computeNextPosition();
            if (newHypotheticalPosition.equals(position)) {
                this.position = newHypotheticalPosition;
            } else {
                // Lawn is a synchronized resource
                lawn.lockLawn();
                if (!lawn.isPositionAvailable(newHypotheticalPosition)) {
                    LOG.warn("Collision with {}, on cell {}", this, newHypotheticalPosition);
                } else {
                    lawn.confirmNewPosition(position, newHypotheticalPosition);
                    this.position = newHypotheticalPosition;
                }
                lawn.unlockLawn();
            }
        }
        return newHypotheticalPosition;
    }

    private Position computeNextPosition() {
        Movement instruction = instructions.poll();
        if (instruction == null) {
            return null;
        }
        if (instruction == Movement.F) {
            // Has to move obviously
            Position newPos = position.getDirection().getMoveForward().apply(position);
            if (!lawn.isCorrectCoordinate(newPos.getCoordinates())) {
                LOG.debug("New position will be discarded because it is outside of the lawn {}", position);
                return position;
            }
            LOG.debug("New position will be {}", newPos);
            return newPos;
        } else {
            Direction newDirection = instruction.getComputeNewDirection().apply(position.getDirection());
            LOG.debug("{}; instruction is {}, direction will be {}", this, instruction, newDirection);
            position.setDirection(newDirection);
        }
        return position;
    }

    private boolean hasNextPosition() {
        return !instructions.isEmpty();
    }

    @Override
    public String toString() {
        return "Mower{" +
                "position=" + position +
                ", instructions=" + instructions +
                '}';
    }

    public void setInstructions(Queue<Movement> instructions) {
        this.instructions = instructions;
    }

    public Mower(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return this.position;
    }

}
