package com.tblasig.mower.model;

import java.util.function.Function;

/***
 * Represents a movement which is a letter in the instruction string ('LFFFFRF').
 */
public enum Movement {

    F(direction -> direction),
    R(Direction::getRight),
    L(Direction::getLeft);

    private Function<Direction, Direction> computeNewDirection;

    Movement(Function<Direction, Direction> computeNewDirection){
        this.computeNewDirection = computeNewDirection;
    }

    public Function<Direction, Direction> getComputeNewDirection() {
        return computeNewDirection;
    }

}
