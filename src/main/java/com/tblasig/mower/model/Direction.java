package com.tblasig.mower.model;

import java.util.function.Function;

/***
 * Represents a cardinal direction. Holds also the left and right cardinal.
 */
public enum Direction {
    N,E,W,S;

    static {
        N.right = Direction.E;
        N.left = Direction.W;
        N.moveForward = position -> new Position(position.getCoordinates().getX(), position.getCoordinates().getY() + 1, position.getDirection());

        E.right = Direction.S;
        E.left = Direction.N;
        E.moveForward = position -> new Position(position.getCoordinates().getX() + 1, position.getCoordinates().getY(), position.getDirection());

        W.right = Direction.N;
        W.left = Direction.S;
        W.moveForward = position -> new Position(position.getCoordinates().getX() - 1, position.getCoordinates().getY(), position.getDirection());

        S.right =  Direction.W;
        S.left = Direction.E;
        S.moveForward = position -> new Position(position.getCoordinates().getX(), position.getCoordinates().getY() - 1, position.getDirection());
    }

//    I think it is this only way to do something like above, which is of course not understand by the compiler
//    E(Direction.N, Direction.S),
//    W(Direction.W, Direction.E),
//    N(Direction.W, Direction.E),
//    S(Direction.W, Direction.E),

    private Direction left;

    private Direction right;

    private Function<Position, Position> moveForward;

    Direction() {

    }

    public Direction getLeft() {
        return left;
    }


    public Direction getRight() {
        return right;
    }

    public Function<Position, Position> getMoveForward() {
        return moveForward;
    }

}
