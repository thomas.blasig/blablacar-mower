package com.tblasig.mower.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * Represents a mower in the lawn. Holds a position and a direction.
 */
public class Position {

    public static final Pattern POSITION_PATTERN = Pattern.compile("(\\d+ \\d+) ([NEWS])");

    private Coordinates2D coordinates;

    private Direction direction = null;

    public Position() {
    }

    public Position(int x, int y, Direction d) {
        coordinates = new Coordinates2D(x, y);
        direction = d;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Coordinates2D getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates2D coordinates) {
        this.coordinates = coordinates;
    }

    public static Position fromString(String line) {
        Matcher matcher = POSITION_PATTERN.matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(String.format("Initial position %s is not well formatted", line));
        }
        Position position = new Position();
        position.setCoordinates(Coordinates2D.fromString(matcher.group(1)));
        position.setDirection(Direction.valueOf(matcher.group(2)));
        return position;
    }

    @Override
    public String toString() {
        return String.format("%d %d %s", coordinates.getX(), coordinates.getY(), direction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return coordinates.equals(position.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates);
    }
}

