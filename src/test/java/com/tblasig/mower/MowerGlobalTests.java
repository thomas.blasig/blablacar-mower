package com.tblasig.mower;

import com.tblasig.mower.service.MowerController;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

/**
 * Those tests are E2E tests (not unit test). Test are done by mocking the input and validate that the output is correct.
 */
public class MowerGlobalTests {

    private PrintStream sysOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setupSystemOut() {
        sysOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void revertStreams() {
        System.setOut(sysOut);
    }

    @Test
    public void nominalTest() {
        String path = "src/test/resources/input-nominal.txt";
        File file = new File(path);
        new MowerController(file).start();
        Assertions.assertEquals("1 3 N\n5 1 E", outContent.toString().trim());
    }

    @Test
    public void inputIncorrectTest() {
        String path = "src/test/resources/input-incorrect.txt";
        File file = new File(path);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new MowerController(file).start();
        });
    }

    @DisplayName("Ensure that order is kept")
    @RepeatedTest(200)
    public void inputCorrectOrder() {
        String path = "src/test/resources/input-order.txt";
        File file = new File(path);

        new MowerController(file).start();
        Assertions.assertEquals("0 0 N\n0 1 N", outContent.toString().trim());
    }

    @Test
    public void inputCorrectHandlingColision() {
        String path = "src/test/resources/input-collision.txt";
        File file = new File(path);

        new MowerController(file).start();
        Assertions.assertEquals("2 4 E\n3 3 E", outContent.toString().trim());
    }

    @Test
    public void inputCorrectHandlingOutOfGrid() {
        String path = "src/test/resources/input-outofgrid.txt";
        File file = new File(path);

        new MowerController(file).start();
        Assertions.assertEquals("5 5 E", outContent.toString().trim());
    }

}
