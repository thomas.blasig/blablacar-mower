package com.tblasig.mower.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DirectionTest {


    @Test
    public void shouldHandleNorthCorrectly() {
        Direction north = Direction.N;
        Position initialPosition = new Position(2,4, north);
        assertEquals(Direction.W, north.getLeft());
        assertEquals(Direction.E, north.getRight());
        assertEquals( new Position(2, 5, Direction.N) ,north.getMoveForward().apply(initialPosition));
    }

    @Test
    public void shouldHandleSouthCorrectly() {
        Direction south = Direction.S;
        Position initialPosition = new Position(2,4, south);
        assertEquals(Direction.E, south.getLeft());
        assertEquals(Direction.W, south.getRight());
        assertEquals( new Position(2, 3, Direction.S) ,south.getMoveForward().apply(initialPosition));
    }

    @Test
    public void shouldHandleWestCorrectly() {
        Direction west = Direction.W;
        Position initialPosition = new Position(2,4, west);
        assertEquals(Direction.S, west.getLeft());
        assertEquals(Direction.N, west.getRight());
        assertEquals( new Position(1, 4, Direction.W) ,west.getMoveForward().apply(initialPosition));
    }

    @Test
    public void shouldHandleEastCorrectly() {
        Direction east = Direction.E;
        Position initialPosition = new Position(2,4, east);
        assertEquals(Direction.N, east.getLeft());
        assertEquals(Direction.S, east.getRight());
        assertEquals( new Position(3, 4, Direction.E) ,east.getMoveForward().apply(initialPosition));
    }
}
