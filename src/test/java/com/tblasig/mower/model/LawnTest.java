package com.tblasig.mower.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LawnTest {




    @Test
    public void shouldAddMower() {

    }


    @Test
    public void shouldControlCorrectlyCoordinates() {


        Coordinates2D cMax =  new Coordinates2D(5,5);
        Lawn lawn = new Lawn(cMax);

        Mower mCorrect = new Mower(new Position(5,5,Direction.E));
        Mower mCorrect2 = new Mower(new Position(0,0,Direction.E));
        Mower mIncorrect = new Mower(new Position(5,6,Direction.E));
        Mower mIncorrect2 = new Mower(new Position(6,5,Direction.E));
        Mower mIncorrect3 = new Mower(new Position(-1,0,Direction.E));
        Mower mIncorrect4 = new Mower(new Position(0,-1,Direction.E));
        Mower mIncorrect5 = new Mower(new Position(7,7,Direction.E));

        Assertions.assertDoesNotThrow(() -> {
            lawn.addMower(mCorrect);
            lawn.addMower(mCorrect2);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            lawn.addMower(mIncorrect);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            lawn.addMower(mIncorrect2);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            lawn.addMower(mIncorrect3);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            lawn.addMower(mIncorrect4);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            lawn.addMower(mIncorrect5);
        });




    }

}
