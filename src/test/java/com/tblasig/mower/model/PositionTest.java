package com.tblasig.mower.model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PositionTest {

    private static final String NORMAL_POSITION = "10 15 E";

    private static final String WRONG_POSITION_1 = "1010E";

    private static final String WRONG_POSITION_2 = "10 10";

    private static final String WRONG_POSITION_3 = "1010";


    @Test
    public void shouldBeCreatedByAString() {
        Position p = Position.fromString(NORMAL_POSITION);
        assertEquals(new Position(10,15, Direction.E), p);
    }

    @Test
    public void shouldFailBecausePositionIsIncorrect() {
        assertThrows(IllegalArgumentException.class, () -> {
            Position.fromString(WRONG_POSITION_1);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            Position.fromString(WRONG_POSITION_2);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            Position.fromString(WRONG_POSITION_3);
        });
    }


    /**
     * Important test
     */
    @Test
    public void shouldBeEqualsOnlyOnCoordinatesNotDirection() {
        Position p1 = new Position(10, 10, Direction.E);
        Position p2 = new Position(10, 10, Direction.S);
        Position p3 = new Position(10, 10, null);
        Position p4 = new Position(5,10, Direction.N);
        assertEquals(p1, p2);
        assertEquals(p2, p3);
        assertNotEquals(p1, p4);
    }


    @Test
    public void shouldBeDisplayedCorrectlyInString() {
        Position p1 = new Position(10, 10, Direction.E);
        Position p2 = new Position(100000, 4150, Direction.N);

        assertEquals("10 10 E", p1.toString());
        assertEquals("100000 4150 N", p2.toString());
    }

}
