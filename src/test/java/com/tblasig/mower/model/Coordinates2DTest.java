package com.tblasig.mower.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Coordinates2DTest {


    @Test
    public void shouldCompareCorrectlyCoordinates() {
        Coordinates2D c1 = new Coordinates2D(0, 0);
        Coordinates2D c2 = new Coordinates2D(1, -1);
        Coordinates2D c3 = new Coordinates2D(-1, 1);
        Coordinates2D c4 = new Coordinates2D(-2, -2);
        // In the "grid point of view " both c2 and c3 are greater than c1.

        assertTrue(c2.isGreaterThan(c1));
        assertTrue(c3.isGreaterThan(c1));
        assertFalse(c4.isGreaterThan(c1));
    }


    @Test
    public void shouldBeCreatedCorrectlyFromString() {
        String normalCoord = "0 0";
        String incorrectCoord = "0";
        String incorectCoord2 = "00";
        String incorectCoord3 = "";

        assertEquals(Coordinates2D.fromString(normalCoord), new Coordinates2D(0, 0));

        assertThrows(IllegalArgumentException.class, () -> {
            Coordinates2D.fromString(incorrectCoord);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            Coordinates2D.fromString(incorectCoord2);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            Coordinates2D.fromString(incorectCoord3);
        });
    }
}
