# Mower exercise

## Install it 
```
./gradlew build
```

## Run in standalone
After the build (build creates a fat jar):
```
java -jar build/libs/mower-exercise-1.0.0.jar input.txt
```
You can replace the file by your input file. 


## Run the tests
```
./gradlew test
```

## Turn on debug logs
Open the src/main/resources/log4j2.xml and replace 'ERROR' by 'DEBUG' as follow in the root logger:
```
<?xml version="1.0" encoding="UTF-8"?>
<Configuration>
    <Appenders>
        <Console name="STDOUT" target="SYSTEM_OUT">
            <PatternLayout pattern="%d %-5p [%t] %C{2} (%F:%L) - %m%n"/>
        </Console>
    </Appenders>
    <Loggers>
        <Logger name="org.apache.log4j.xml" level="ERROR"/>
        <Root level="DEBUG">
            <AppenderRef ref="STDOUT"/>
        </Root>
    </Loggers>
</Configuration>
```